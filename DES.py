while True:
    try:
        message = input('Give message to encrypt, done to exit: ')
        if message == 'done':
            break

        key = input('Give 8 character long key: ')
        while len(key) != 8:
            key = input('Key is of wrong size! It must be exactly 8 characters long! Enter key: ')

        else:
            def bitstream_f(message):
                    bitstream = ''.join(format(i, '08b') for i in bytearray(message, encoding='utf-8'))
                    stlen = len(bitstream)
                    #print('string length is: ' + str(stlen))
                    stlef = stlen % 64
                    #print('incomplete 8bit chunks: ' + str(stlef))
                    #print(bitstream)

                    if stlef is not 0:
                        bitstream = ''.join('0' * (64 - stlef)) + bitstream
                        #print(bitstream)
                        #print(len(bitstream))
                    return bitstream

            def block(bitstream):
                length = len(bitstream)
                blocks = []
                start = 0
                for i in range(64, length, 64):
                    blocks.append(bitstream[start:i])
                    print(len(bitstream[start:i]))
                    start = i
                print(blocks)
                #print((len(blocks)))
                return blocks

            def keystream_f(key):
                keystream = ''.join(format(i, '08b') for i in bytearray(key, encoding='utf-8'))
                klen = len(keystream)
                #print('key length is: ' + str(klen))

                #klef = klen % 64
                #print('incomplete 8bit chunks: ' + str(klef))
                #print(keystream)

                keystring = keystream[0:7] + keystream[8:15] + keystream[16:23] + keystream[24:31] + keystream[32:39] + keystream[40:47] + keystream[48:55] + keystream[56:63]
                #print(keystring)
                #print(len(keystring))
                return keystring

            def init_permutation(bitstream):
                init1 = [58, 50, 42, 34, 26, 18, 10, 2, 60, 52, 44, 36, 28, 20, 12, 4, 62, 54, 46, 38, 30, 22, 14, 6, 64, 56, 48, 40, 32, 24, 16, 8, 57, 49, 41, 33, 25, 17, 9, 1, 59, 51, 43, 35, 27, 19, 11, 3, 61, 53, 45, 37, 29, 21, 13, 5, 63, 55, 47, 39, 31, 23, 15, 7]
                #print(lpc1[2][3])

                pmessage = list(bitstream)
                #print(pmessage)
                #print(pmessage[57])
                #print(pmessage[0])
                for i in range(0, 64):
                    pmessage[i] = bitstream[init1[i] - 1]
                #print(pmessage)

                lpmessage = pmessage[0:32]
                #print(lpmessage, end='')
                rpmessage = pmessage[32:64]
                #print(rpmessage)

                return lpmessage, rpmessage

            def init_key_permutation(keystring):
                lpc1 = [57, 49, 41, 33, 25, 17, 9, 1, 58, 50, 42, 34, 26, 18, 10, 2, 59, 51, 43, 35, 27, 19, 11, 3, 60, 52, 44, 36, 63, 55, 47, 39, 31, 23, 15, 7, 62, 54, 46, 38, 30, 22, 14, 6, 61, 53, 45, 37, 29, 21, 13, 5, 28, 20, 12, 4]
                # print(lpc1[2][3])

                pkey = list(keystring)
                # print(pmessage)
                # print(pmessage[57])
                # print(pmessage[0])
                for i in range(0, 56):
                    pkey[i] = key[lpc1[i] - 1]
                # print(pmessage)

                lpkey = pkey[0:28]
                # print(lpmessage, end='')
                rpkey = pkey[28:56]
                # print(rpmessage)

                return lpkey, rpkey

            def ebox_f(rpmessage):
                ebox = [32, 1, 2, 3, 4, 5, 4, 5, 6, 7, 8, 9, 8, 9, 10, 11, 12, 13, 12, 13, 14, 15, 16, 17, 16, 17, 18, 19, 20, 21, 20, 21, 22, 23, 24, 25, 24, 25, 26, 27, 28, 29, 28, 29, 30, 31, 32, 1]
                #print(len(ebox))
                #No message partitioning needed because of the preconstructed table.
                rpnew = []
                for i in range(0, 48):
                    rpnew.append(rpmessage[ebox[i] - 1])
                    #print(rpnew)
                #print(len(rpnew))
                return rpnew

            '''def key_schedule(shifted_keystring):
                per_key = []
                key_per = [14, 17, 11, 24, 1, 5, 3, 28, 15, 6, 21, 10, 23, 19, 12, 4, 26, 8, 16, 7, 27, 20, 13, 2, 41, 52, 31, 37, 47, 55, 30, 40, 51, 45, 33, 48, 44, 49, 39, 56, 34, 53, 46, 42, 50, 36, 29, 32]
                for i in range(0, 48):
                    per_key.append(shifted_keystring[key_per[i]-1])
                #print(len(per_key))
                return per_key

            def key_scedule_round(lpkey, rpkey):
               bad_numbers = [1, 2, 9, 16]
               for i in range(0, 16):
                   if i in bad_numbers:'''



            #def rounds(lpmessage, rpmessage, keystring):

            '''for i in range(len(blocks[i])):
                for round in range(0, 16):
                    lm,rm = init_permutation(blocks[])
                    rounds()'''



            #bitstream_f(message)
            #keystream_f(key)
            #print('before')
            lm, rm = init_permutation(bitstream_f(message))
            #print(lm)
            #print(rm)
            #print('after')
            #init_permutation(bitstream_f(message))
            ebox_f(rm)
            print('before')
            #key_schedule(keystream_f(key))
            block(bitstream_f(message))
            print('after')
            #print('really after')
            #print(lm)
            #print(len(rm))


    except:
        print('Invalid input!')
        continue

'''for i in bitstream:
    blocklist = '''

